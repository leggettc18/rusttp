# rusttp

## A very simple multithreaded http server

If you've read Chapter 20 of the rust book, this will look
familiar. I am working on ways to expand it and refactor
it into something perhaps a bit more useful, but it will
probably always be fairly simple in terms of feature set.

For now, if you want to run it (for some reason) you can
clone the repository and run 

```
cargo run
```

in the project's root directory. If you then navigate to
`127.0.0.1:7878` in your browser you will get the
`hello.html` page. If you add `/sleep` to the end you will
get the same page but it takes 5 seconds to load. If you
add anything else other than `/sleep` to the end you will
get `404.html`. As you can see, not exactly featureful.

In the future, I plan on adding:

- A simple routing engine
- Some configuration options
- More robust error handling

It probably won't ever be the most full featured http server
in the world but it could at least be useful for testing
purposes I suppose.